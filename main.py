from flask import Flask
from flask import render_template
import requests
import json

app = Flask(__name__)






# Route for index page(home page)
@app.route("/", methods=["GET", "POST"])
# function for index page
def index():
    # returning index page
    return render_template("index.html")


@app.route("/foundationdiploma", methods=["GET", "POST"])
def foundationdiploma():
    req_top = requests.get('https://script.google.com/macros/s/AKfycbxOWskfzeRSMi6P-O1Ts5C2CpMNc7KsiRZisYxvQwJdXmVaKg6N0MRE9WTXeWssjdPc/exec')
    tops = json.loads(req_top.content)
    req_com = requests.get('https://script.google.com/macros/s/AKfycbzQMisXj_wEjnTMv4xp91-8AEY3ICIsdTZUdH7RSg4CFa7FzyRTnYBo0ps2rRECEQVC/exec')
    top_com = json.loads(req_com.content)
    req_coaa = requests.get('https://script.google.com/macros/s/AKfycby-S9vDtJe111OuvUkCK58gvecsTtj7sC0A7a-yQPrwLW431Sc1ahXSkKjg1ZLvpfIu/exec')
    top_coaa = json.loads(req_coaa.content)
    return render_template("foundationdiploma.html", tops=tops['data'],top_com=top_com['data'], top_coaa=top_coaa['data'])


@app.route("/degree", methods=["GET", "POST"])
def degree():
    req_top = requests.get('https://script.google.com/macros/s/AKfycbx2HKRLKDoE95Ia1ZDCBc66UcjE3GMg6W9dzXoU7cOmE1B345_5c_lnEk2QBuAFCvrt/exec')
    tops = json.loads(req_top.content)
    req_com = requests.get('https://script.google.com/macros/s/AKfycbx_3xbk-w5tQHcQhwhFfrRQCXcEb1KrVsWBAmulIfsINZw-PuHyQoAiL_iWVSK0hape/exec')
    top_com = json.loads(req_com.content)
    req_coaa = requests.get('https://script.google.com/macros/s/AKfycbzGnJ-29-dzKb49HVxdBaMZefGsr7nAwyib5fCFgwO-W5cOQVVuh4eOV4s6HlWA5hte/exec')
    top_coaa = json.loads(req_coaa.content)
    return render_template('degree.html', tops=tops['data'], top_com=top_com['data'], top_coaa=top_coaa['data'])



@app.route("/directdiploma", methods=["GET", "POST"])
def directdiploma():
    req_top_p = requests.get('https://script.google.com/macros/s/AKfycbzXE3uXzSAydV2FER3DHxeeHUzVVxUb6LNoCDywAJCUfXKQmqyNcwetSXdMpqVNSEdt/exec')
    tops_p = json.loads(req_top_p.content)
    req_top_ds = requests.get('https://script.google.com/macros/s/AKfycbyCqn9nJmU20ADgIjfbBkCRAYwjkI-_KY6CP6biVuaM_jDv6zIpeJeIXkfazBs13TYM/exec')
    tops_ds = json.loads(req_top_ds.content)
    
    return render_template("directdiploma.html",  tops_p=tops_p['data'], tops_ds=tops_ds['data'])



if __name__ == '__main__':
    app.run(debug=True, port=5000)
